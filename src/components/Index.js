import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import Arg from '../Images/flags/argentina-flag-xs.png';
import Usa from '../Images/flags/united-states-of-america-flag-xs.png';


const Index = () => {
    return (
        <Container className="text-center text-dark">
            <h1>Elegi tu pais / Choose your country</h1>
            <Row className="mt-5">
                <Col className="">
                    <a href="/noticias/business/ar" >
                        <Image src={Arg} width="280" />
                    </a>
                </Col>
                <Col>
                    <a href="/noticias/business/us">
                        <Image src={Usa} width="340" />
                    </a>
                </Col>
            </Row>

        </Container>
    );
};

export default Index;