import React from 'react';
import { Image, Col } from 'react-bootstrap'

const Noticia = (noticia) => {

    const { title, description, urlToImage } = noticia.noticia

    return (

        <Col md={4}>
            <div className="text-left m-2">
                <Image src={urlToImage} alt=".." width="300" height="200" />
                <h6>{title}</h6>
                <p>{description}</p>
            </div>
        </Col>

    );
};

export default Noticia;