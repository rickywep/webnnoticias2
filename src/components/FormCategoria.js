import React, { useState, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import { Redirect } from "react-router";
import { useParams } from "react-router";


// import {Link} from 'react-router-dom';
const FormCategoria = () => {

    const [categorias, setCategorias] = useState()

    let {pais} =  useParams();




    useEffect(() => {
        console.log(categorias);


    }, [categorias])

    const selectCategory = e => {
        setCategorias(e.target.value)

    }

    return (
        <>
            {categorias==="business" ? <Redirect to={`/noticias/${categorias}/${pais}`}/> : null}
            {categorias==="entertainment" ? <Redirect to={`/noticias/${categorias}/${pais}`}/> : null}
            {categorias==="general" ? <Redirect to={`/noticias/${categorias}/${pais}`}/> : null}
            {categorias==="health" ? <Redirect to={`/noticias/${categorias}/${pais}`}/> : null}
            {categorias==="science" ? <Redirect to={`/noticias/${categorias}/${pais}`}/> : null}
            {categorias==="sports" ? <Redirect to={`/noticias/${categorias}/${pais}`}/> : null}
            {categorias==="technology" ? <Redirect to={`/noticias/${categorias}/${pais}`}/> : null}
            
            <Form>
                <Form.Group controlId="exampleForm.ControlSelect1">
                    <Form.Label>Categorias</Form.Label>
                    <Form.Control as="select" onChange={selectCategory} value={categorias}>
                        <option>business</option>
                        <option>entertainment</option>
                        <option>general</option>
                        <option>health</option>
                        <option>science</option>
                        <option>sports</option>
                        <option>technology</option>
                    </Form.Control>
                </Form.Group>
            </Form>
        </>
    );
};

export default FormCategoria;