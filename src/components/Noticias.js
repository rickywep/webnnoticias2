import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Noticia from './Noticia';
import { Row, Container } from 'react-bootstrap'
import { useParams } from "react-router";
import FormCategoria from './FormCategoria';



const Noticias = () => {

    const [noticias, setNoticias] = useState([])

    let {categoria, pais} =  useParams();
    // let category =  "general";
    let url= `http://newsapi.org/v2/top-headlines?country=${pais}&category=${categoria}&apiKey=3b16ba836a2f40979f43e7f8d5f21cc7`

    useEffect(() => {
        queryAPI()
        console.log("params", categoria, pais);
        console.log(url);
        


    }, [categoria])

    const queryAPI = async () => {
        await axios.get(url)
            .then(res => {
                setNoticias(res.data.articles)
                
            })
    };

    return (
        <Container>
            <FormCategoria/>
            <Row>
                {
                    noticias.map((noticia, id) => (
                        <Noticia
                            key={id}
                            noticia={noticia}
                        />
                    ))
                }
            </Row>
        </Container>
    );
};

export default Noticias;