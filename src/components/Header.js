import React from 'react';
import { Nav, Image } from 'react-bootstrap'
import Arg from '../Images/flags/argentina-small.png'
import Usa from '../Images/flags/usa-small.png'


const Header = () => {
    return (
        <Nav className="bg-dark text-light">
            <Nav.Item>
                <Nav.Link href="/noticias/business/ar">
                    <Image src={Arg} width="50" />
                </Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link href="/noticias/business/us">
                    <Image src={Usa} width="50" />
                </Nav.Link>
            </Nav.Item>
            <div className="m-auto">
                <a href="/">
                    <h1 className="mr-5"> Noticias</h1>
                </a>
            </div>
        </Nav>
    );
};

export default Header;