import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Header';
import Noticias from './components/Noticias';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Index from './components/Index';


function App() {
  return (
    <Router>
      <div className="">
        <Header />
        <div className="container mt-5">
          <Switch>
            <Route exact path="/noticias/:categoria/:pais" component={Noticias} />
            <Route exact path="/" component={Index} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
